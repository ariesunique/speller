class Config(object):
    """
    Common configurations
    """

class DebugConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = True
    LOGLEVEL = "DEBUG"

class TestConfig(Config):
    pass
    
class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = True
    LOGLEVEL = "INFO"

class ProductionConfig(Config):
    DEBUG = False
    LOGLEVEL = "ERROR"

app_config = {
    'debug':DebugConfig,
    'development': DevelopmentConfig,
    'production': ProductionConfig
}