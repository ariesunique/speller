from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login_manager

class User(UserMixin, db.Model):
    __tablename__ = 'users'
    
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(60), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    
    @property
    def password(self):
        """
        Prevent password from being accessed
        """
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)
        
    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)
        
    def __repr__(self):
        return '<Employee: {}>'.format(self.username)

        
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
        
        
class Teacher(db.Model):
    __tablename__ = 'teachers'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(20), index=True)
    last_name = db.Column(db.String(20), index=True)
    email = db.Column(db.String(60), index=True, unique=True)
    phone = db.Column(db.String(20))
    class_id = db.Column(db.Integer, db.ForeignKey('classes.id'))
    #middle_name = db.Column(db.String(20), index=True)
    #classroom = db.relationship("Classroom", uselist=False, back_populates="teacher")

    def __repr__(self):
        return '<Teacher: {} {}>'.format(self.first_name, self.last_name)


class Classroom(db.Model):
    __tablename__ = 'classes'

    id = db.Column(db.Integer, primary_key=True)
    level = db.Column(db.Integer, index=True)
    room = db.Column(db.String(10))
    #teacher_id = db.column(db.Integer, db.ForeignKey('teachers.id'))
    teacher = db.relationship("Teacher", backref="classroom", lazy='dynamic')

    def __repr__(self):
        return '<Classroom: Level {}, Teacher {}>'.format(self.level, self.teacher_id)
