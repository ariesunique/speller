from flask import flash, redirect, render_template, url_for
from flask_login import login_required

from . import home

@home.route('/')
@home.route('/index')
def index():
    return redirect(url_for('auth.login'))
    
    
@home.route('/dashboard')
@login_required
def dashboard():
    return render_template('home/dashboard.html')
