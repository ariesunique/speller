import os

from flask import flash, Flask, redirect, render_template, url_for
from flask_login import login_required, login_user, logout_user
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from app import create_app
from app import db

config_name = os.getenv('FLASK_CONFIG', 'development')
app = create_app(config_name)

from app.models import Teacher
from app.forms import TeacherForm

@app.route('/list')
@login_required
def list():
    teachers = Teacher.query.all()
    return render_template('list.html', teachers=teachers)

@app.route('/teachers/add', methods=['GET', 'POST'])
def add_teacher():
    form = TeacherForm()
    if form.validate_on_submit():
        teacher = Teacher(first_name=form.first_name.data, last_name=form.last_name.data, email=form.email.data)
        try:
            db.session.add(teacher)
            db.session.commit()
            flash('You have successfully added a new teacher.')
        except:
            flash('Error: teacher already exists')
        return redirect(url_for('list'))

    return render_template('add_teacher.html', form=form)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
    
@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

if __name__ == "__main__":
    app.run()